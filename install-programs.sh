#!/bin/bash

echo "Installing Homebrew"
ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

echo "Installing bundle for installing latest dumped Brewfile"
brew tap Homebrew/bundle
brew bundle

echo "Setting up aws cli commands"
sudo easy_install pip
sudo pip install awscli
