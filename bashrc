# #!/bin/bash

#-------------------------------------------------------------------------------
# Stijn Symons dotfiles
#-------------------------------------------------------------------------------

# Basics
: ${HOME=~}
: ${LOGNAME=$(id -un)}
: ${UNAME=$(uname)}
: ${DEFAULT_USERNAME='stijn'}

# Proper locale
: ${LANG:="en_US.UTF-8"}
: ${LANGUAGE:="en"}
: ${LC_CTYPE:="en_US.UTF-8"}
: ${LC_ALL:="en_US.UTF-8"}

# Fucking mail notifications
unset MAILCHECK

export LANG LANGUAGE LC_CTYPE LC_ALL

# editor
export EDITOR=vim

#increase history size
export HISTSIZE=9999

# Update window size after every command
shopt -s checkwinsize

#-------------------------------------------------------------------------------
# Prompt
#-------------------------------------------------------------------------------
export LSCOLORS="gxcxfxdxbxegedabagacad"
export CLICOLOR=1

function parse_git_dirty() {
    git diff --quiet --ignore-submodules HEAD 2>/dev/null; [ $? -eq 1 ] && echo '*'
}

function parse_git_branch() {
    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/ \1$(parse_git_dirty)/"
}


if [ "$TERM_PROGRAM" == "iTerm.app" ]; then
    export PROMPT_COMMAND=__prompt_command  # Func to gen PS1 after CMDs
fi

function __prompt_command() {
    local EXIT="$?"             # This needs to be first
    PS1=""

    local YELLOW=$(printf "\[\e[33;1m\]")
    local GREEN=$(printf "\[\e[32;1m\]")
    local BLUE=$(printf "\[\e[34;1m\]")
    local GREY=$(printf "\[\e[0m\]")
    local LIGHT_CYAN=$(printf "\[\033[1;36m\]")
    local DARK_GREY=$(printf "\[\e[38;05;241m\]")
    local RED=$(printf "\[\e[1;31m\]")

    if [ $USER == "vagrant" ]; then
        PS1+="${LIGHT_CYAN}\u "
    fi

    PS1+="${GREEN}\w${DARK_GREY}\$(parse_git_branch)${GREEN}"

    if [ $EXIT != 0 ]; then
        PS1+=" ${RED}✖";
    else
        PS1+=" →";
    fi

    PS1+=" ${GREY}"
}

#-------------------------------------------------------------------------------
# Path
#-------------------------------------------------------------------------------
export PATH=/usr/local/opt/postgresql@13/bin:/usr/local/opt/php@7.4/bin:/usr/local/opt/php@7.4/sbin:/usr/local/bin:~/.composer/vendor/bin/:~/bin:/usr/local/sbin:/Users/stijn/.local/bin:$PATH

#-------------------------------------------------------------------------------
# Aliases
#-------------------------------------------------------------------------------

# shell
LS_OPTIONS=""
if [ "$UNAME" == "Linux" ]; then
    LS_OPTIONS="--color"
fi
alias l='ls -lAhF $LS_OPTIONS'
alias ll='ls -lAhF $LS_OPTIONS'
alias dir='ls -ld $LS_OPTIONS'
alias ff='find . -name "*" -type f | xargs grep '

# git
alias g='git add . && git commit && git push'
alias gs='git status -sb'
alias gd='git diff'
alias gb='git branch'
alias ga='git add'
alias gc='git commit'
#alias gp='git push'
alias gco='git checkout'

# editor aliases
if [ "$UNAME" == "Darwin" ]; then
    if [ ! -f "/usr/local/bin/subl" ]; then
    	if [ -f /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl ]; then
            echo "Linking "
    		sudo ln -s "/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl" /usr/local/bin/subl
    	fi
    fi
    alias s='subl'
fi

# misc
alias password='php -r "\$password=PHP_EOL;for(\$length=0;\$length<12;\$length++){\$password.=chr(rand(40,122));}echo \$password.PHP_EOL.PHP_EOL;"'

# alias for vscode to avoid duplicate dock icons
alias code='open -b com.microsoft.VSCode "$@"'

# adx
alias lightingest="dotnet lightingest/Microsoft.Azure.Kusto.Tools.NETCore.5.4.2/tools/LightIngest.dll"
#-------------------------------------------------------------------------------
# Autocompleters
#-------------------------------------------------------------------------------
# bash-completion@2
# if [ -f /usr/local/share/bash-completion/bash_completion ]; then
#     . /usr/local/share/bash-completion/bash_completion
# fi

[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"

#-------------------------------------------------------------------------------
# Saki
#-------------------------------------------------------------------------------
export SAKI_HOME=~/saki/
export AWS_PROFILE=appstrakt

#-------------------------------------------------------------------------------
# Fuzzy finder (https://github.com/junegunn/fzf)
#-------------------------------------------------------------------------------
force_color_prompt=yes
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

#-------------------------------------------------------------------------------
# Colored man pages
#-------------------------------------------------------------------------------
man() {
    env \
    LESS_TERMCAP_mb=$(printf "\e[1;31m") \
    LESS_TERMCAP_md=$(printf "\e[1;31m") \
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_se=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[1;32m") \
    man "$@"
}

#-------------------------------------------------------------------------------
# NVM config
#-------------------------------------------------------------------------------
# export NVM_DIR="~/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
export NVM_DIR="$HOME/.nvm"
[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"
[ -s "/usr/local/opt/nvm/etc/bash_completion" ] && . "/usr/local/opt/nvm/etc/bash_completion"

#-------------------------------------------------------------------------------
# golang
#-------------------------------------------------------------------------------

export GOPATH=$HOME/go
if [ "$UNAME" == "Darwin" ]; then
	export PATH=$PATH:/usr/local/opt/go/libexec/bin:$GOPATH/bin
	export GOROOT=/usr/local/opt/go/libexec
fi

#-------------------------------------------------------------------------------
# High sierra ssh
#-------------------------------------------------------------------------------

ssh-add -K ~/.ssh/id_rsa > /dev/null 2>&1

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export HOMEBREW_GITHUB_API_TOKEN="4ee171bafee8c6f041e609ac59ba8c4e4dd5ce1b"
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[ -f /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.bash ] && . /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.bash
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[ -f /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.bash ] && . /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.bash

#-------------------------------------------------------------------------------
# Spencer
#-------------------------------------------------------------------------------
alias azsize="echo 'service,env,stack,role,size' && az vm list | jq -r '.[] | [\"vm\", .tags.environment,.tags.infrastructure,.name,.hardwareProfile.vmSize]|@csv' && az vmss list | jq -r '.[]  | [\"vmss\", .tags.environment,.tags.infrastructure,.tags.role,.sku.name]|@csv' && az mysql server list | jq -r '.[] | [\"mysql\", .tags.infrastructure, .tags.environment, \"\",.sku.name]|@csv' && az redis list | jq -r '.[] | [\"redis\", .tags.infrastructure, .tags.environment, \"\", ([.sku.family, .sku.capacity] | join(\"-\"))]|@csv'"

#-------------------------------------------------------------------------------
# Spencer
#-------------------------------------------------------------------------------

export GITHUB_TOKEN="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"

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
