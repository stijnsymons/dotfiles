--------------------------------------------------------------------------------
-- rtoshiro - https://github.com/rtoshiro
-- You should see: http://www.hammerspoon.org/docs/index.html
--------------------------------------------------------------------------------
-- no keyboard shortcut
-- require "tools/clipboard"
-- require "tools/wifi"

--------------------------------------------------------------------------------
-- CONSTANTS
--------------------------------------------------------------------------------
local hyper = {"cmd", "ctrl"}

--------------------------------------------------------------------------------
-- CONFIGURATIONS
--------------------------------------------------------------------------------
hs.window.animationDuration = 0

--------------------------------------------------------------------------------
-- LAYOUTS
-- SINTAX:
--  {
--    name = "App name" ou { "App name", "App name" }
--    func = function(index, win)
--      COMMANDS
--    end
--  },
--
-- It searches for application "name" and call "func" for each window object
--------------------------------------------------------------------------------
local layouts = {
  {
  -- maximized apps
    name = {"Slack", "Google Chrome", "Spotify", "Sublime Text", "WhatsApp", "iTerm2", "Atom", "Code"},
    func = function(index, win)
      win:fullscreenWidth()
    end
  }
}

function config()
  hs.hotkey.bind(hyper, "l", function()
    local win = hs.window.focusedWindow()
    win:right()
  end)

  hs.hotkey.bind(hyper, "h", function()
    local win = hs.window.focusedWindow()
    win:left()
  end)

  hs.hotkey.bind(hyper, "j", function()
    local win = hs.window.focusedWindow()
    win:up()
  end)

  hs.hotkey.bind(hyper, "k", function()
    local win = hs.window.focusedWindow()
    win:down()
  end)

  hs.hotkey.bind(hyper, "f", function()
    local win = hs.window.focusedWindow()
    win:all()
  end)

  hs.hotkey.bind(hyper, "p", function()
    local win = hs.window.focusedWindow()
    win:upRight()
  end)

  hs.hotkey.bind(hyper, "u", function()
    local win = hs.window.focusedWindow()
    win:upLeft()
  end)

  hs.hotkey.bind(hyper, "n", function()
    local win = hs.window.focusedWindow()
    win:downLeft()
  end)

  hs.hotkey.bind(hyper, ".", function()
    local win = hs.window.focusedWindow()
    win:downRight()
  end)

  hs.hotkey.bind(hyper, "d", function()
    local win = hs.window.focusedWindow()
    win:toggleFullScreen()
  end)

  hs.hotkey.bind(hyper, "s", function()
    hs.alert(hs.spotify.getCurrentArtist()..'\n'..hs.spotify.getCurrentTrack(), hs.alert.defaultStyle, hs.screen.mainScreen(), 2)
  end)

  -- -- lock screen shortcut
  -- hs.hotkey.bind(hyper, "e", function()
  --   hs.caffeinate.startScreensaver()
  -- end)

end

hs.hotkey.bind(hyper, '1', function()
  toggle_application("Google Chrome")
   hs.applescript([[tell application "Finder"
       activate application "Google Chrome"
   end tell
   tell application "Google Chrome"
       -- If an existing tab is gmail, make it the front most tab
       -- (Can find no way to move it to far left, so using auto pin tab extension to do it)
       repeat with w in windows
           set i to 1
           repeat with t in tabs of w
               if URL of t starts with "https://mail.google.com/mail/u/0/" then
                   set active tab index of w to i
                   set index of w to 1
                   return
               end if
               set i to i + 1
           end repeat
       end repeat
       --If none of the existing tabs is gmail, create a new gmail tab
       repeat with w in windows
           set i to 1
           repeat with t in tabs of w
               if URL of t does not start with "https://mail.google.com/mail/u/0/" then
                   make new tab at the beginning of window 1 with properties {URL:"https://mail.google.com/mail/u/0/"}
                   return
               end if
               set i to i + 1
           end repeat
       end repeat
       -- If there are no open windows, then open one and open gmail in it
       if windows = {} then
           make new window
           set URL of (active tab of window 1) to "https://mail.google.com/mail/u/0/"
       end if
   end tell]])
end)
hs.hotkey.bind(hyper, '2', function() toggle_application("Slack") end)
hs.hotkey.bind(hyper, '3', function() toggle_application("Sublime Text") end)

--------------------------------------------------------------------------------
-- END CONFIGURATIONS
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
-- METHODS - BECAREFUL :)
--------------------------------------------------------------------------------
function toggle_application(_app)
    local app = hs.appfinder.appFromName(_app)
    if not app then
        -- FIXME: This should really launch _app
        return
    end
    local mainwin = app:mainWindow()
    if mainwin then
        if mainwin == hs.window.focusedWindow() then
            mainwin:application():hide()
        else
            mainwin:application():activate(true)
            mainwin:application():unhide()
            mainwin:focus()
        end
    end
end

function applyLayout(layouts, app)
  if (app) then
    local appName = app:title()
    for i, layout in ipairs(layouts) do
      if (type(layout.name) == "table") then
        for i, layAppName in ipairs(layout.name) do
          if (layAppName == appName) then
            local wins = app:allWindows()
            local counter = 1
            for j, win in ipairs(wins) do
              if (win:isVisible() and layout.func) then
                layout.func(counter, win)
                counter = counter + 1
              end
            end
          end
        end
      elseif (type(layout.name) == "string") then
        if (layout.name == appName) then
          local wins = app:allWindows()
          local counter = 1
          for j, win in ipairs(wins) do
            if (win:isVisible() and layout.func) then
              layout.func(counter, win)
              counter = counter + 1
            end
          end
        end
      end
    end
  end
end

function applyLayouts(layouts)
  for i, layout in ipairs(layouts) do
    if (type(layout.name) == "table") then
      for i, appName in ipairs(layout.name) do
        local app = hs.appfinder.appFromName(appName)
        if (app) then
          local wins = app:allWindows()
          local counter = 1
          for j, win in ipairs(wins) do
            if (win:isVisible() and layout.func) then
              layout.func(counter, win)
              counter = counter + 1
            end
          end
        end
      end
    elseif (type(layout.name) == "string") then
      local app = hs.appfinder.appFromName(layout.name)
      if (app) then
        local wins = app:allWindows()
        local counter = 1
        for j, win in ipairs(wins) do
          if (win:isVisible() and layout.func) then
            layout.func(counter, win)
            counter = counter + 1
          end
        end
      end
    end
  end
end

function hs.screen.get(screen_name)
  local allScreens = hs.screen.allScreens()
  for i, screen in ipairs(allScreens) do
    if screen:name() == screen_name then
      return screen
    end
  end
end

-- Returns the width of the smaller screen size
-- isFullscreen = false removes the toolbar
-- and dock sizes
function hs.screen.minWidth(isFullscreen)
  local min_width = math.maxinteger
  local allScreens = hs.screen.allScreens()
  for i, screen in ipairs(allScreens) do
    local screen_frame = screen:frame()
    if (isFullscreen) then
      screen_frame = screen:fullFrame()
    end
    min_width = math.min(min_width, screen_frame.w)
  end
  return min_width
end

-- isFullscreen = false removes the toolbar
-- and dock sizes
-- Returns the height of the smaller screen size
function hs.screen.minHeight(isFullscreen)
  local min_height = math.maxinteger
  local allScreens = hs.screen.allScreens()
  for i, screen in ipairs(allScreens) do
    local screen_frame = screen:frame()
    if (isFullscreen) then
      screen_frame = screen:fullFrame()
    end
    min_height = math.min(min_height, screen_frame.h)
  end
  return min_height
end

-- If you are using more than one monitor, returns X
-- considering the reference screen minus smaller screen
-- = (MAX_REFSCREEN_WIDTH - MIN_AVAILABLE_WIDTH) / 2
-- If using only one monitor, returns the X of ref screen
function hs.screen.minX(refScreen)
  local min_x = refScreen:frame().x
  local allScreens = hs.screen.allScreens()
  if (#allScreens > 1) then
    min_x = refScreen:frame().x + ((refScreen:frame().w - hs.screen.minWidth()) / 2)
  end
  return min_x
end

-- If you are using more than one monitor, returns Y
-- considering the focused screen minus smaller screen
-- = (MAX_REFSCREEN_HEIGHT - MIN_AVAILABLE_HEIGHT) / 2
-- If using only one monitor, returns the Y of focused screen
function hs.screen.minY(refScreen)
  local min_y = refScreen:frame().y
  local allScreens = hs.screen.allScreens()
  if (#allScreens > 1) then
    min_y = refScreen:frame().y + ((refScreen:frame().h - hs.screen.minHeight()) / 2)
  end
  return min_y
end

-- Returns the frame of the smaller available screen
-- considering the context of refScreen
-- isFullscreen = false removes the toolbar
-- and dock sizes
function hs.screen.minFrame(refScreen, isFullscreen)
  return {
    x = 0,
    y = hs.screen.minY(refScreen),
    w = hs.screen.minWidth(isFullscreen)+5,
    h = hs.screen.minHeight(isFullscreen)
  }
end

-- +-----------------+
-- |                 |
-- |      HERE       |
-- |                 |
-- +-----------------+
function hs.window.all(win)
  local minFrame = hs.screen.minFrame(win:screen(), false)
  minFrame.w = minFrame.w
  win:setFrame(minFrame)
end

-- +-----------------+
-- |        |        |
-- |        |  HERE  |
-- |        |        |
-- +-----------------+
function hs.window.right(win)
  local minFrame = hs.screen.minFrame(win:screen(), false)
  minFrame.x = minFrame.x + (minFrame.w/3*2)
  minFrame.w = minFrame.w/3
  win:setFrame(minFrame)
end

-- +-----------------+
-- |        |        |
-- |  HERE  |        |
-- |        |        |
-- +-----------------+
function hs.window.left(win)
  local minFrame = hs.screen.minFrame(win:screen(), false)
  minFrame.w = minFrame.w*2/3
  win:setFrame(minFrame)
end

-- +-----------------+
-- |      HERE       |
-- +-----------------+
-- |                 |
-- +-----------------+
function hs.window.up(win)
  local minFrame = hs.screen.minFrame(win:screen(), false)
  minFrame.h = minFrame.h/2
  win:setFrame(minFrame)
end

-- +-----------------+
-- |                 |
-- +-----------------+
-- |      HERE       |
-- +-----------------+
function hs.window.down(win)
  local minFrame = hs.screen.minFrame(win:screen(), false)
  minFrame.y = minFrame.y + minFrame.h/2
  minFrame.h = minFrame.h/2
  win:setFrame(minFrame)
end

-- +-----------------+
-- |  HERE  |        |
-- +--------+        |
-- |                 |
-- +-----------------+
function hs.window.upLeft(win)
  local minFrame = hs.screen.minFrame(win:screen(), false)
  minFrame.w = minFrame.w/3*2
  minFrame.h = minFrame.h/2
  win:setFrame(minFrame)
end

-- +-----------------+
-- |                 |
-- +--------+        |
-- |  HERE  |        |
-- +-----------------+
function hs.window.downLeft(win)
  local minFrame = hs.screen.minFrame(win:screen(), false)
  win:setFrame({
    x = minFrame.x,
    y = minFrame.y + minFrame.h/2,
    w = minFrame.w/3*2,
    h = minFrame.h/2
  })
end

-- +-----------------+
-- |                 |
-- |        +--------|
-- |        |  HERE  |
-- +-----------------+
function hs.window.downRight(win)
  local minFrame = hs.screen.minFrame(win:screen(), false)
  win:setFrame({
    x = minFrame.x + minFrame.w/3*2,
    y = minFrame.y + minFrame.h/2,
    w = minFrame.w/3,
    h = minFrame.h/2
  })
end

-- +-----------------+
-- |        |  HERE  |
-- |        +--------|
-- |                 |
-- +-----------------+
function hs.window.upRight(win)
  local minFrame = hs.screen.minFrame(win:screen(), false)
  win:setFrame({
    x = minFrame.x + minFrame.w/3*2,
    y = minFrame.y,
    w = minFrame.w/3,
    h = minFrame.h/2
  })
end

-- It like fullscreen but with minY and minHeight values
-- +------------------+
-- |                  |
-- +------------------+--> minY
-- |       HERE       |
-- +------------------+--> minHeight
-- |                  |
-- +------------------+
function hs.window.fullscreenWidth(win)
  local minFrame = hs.screen.minFrame(win:screen(), false)
  win:setFrame({
    x = 0,
    y = minFrame.y,
    w = minFrame.w,
    h = minFrame.h
  })
end

function applicationWatcher(appName, eventType, appObject)
  if (eventType == hs.application.watcher.activated) then
    if (appName == "iTerm") then
        appObject:selectMenuItem({"Window", "Bring All to Front"})
    elseif (appName == "Finder") then
        appObject:selectMenuItem({"Window", "Bring All to Front"})
    end
  end

  if (eventType == hs.application.watcher.launched) then
    os.execute("sleep " .. tonumber(1))
    applyLayout(layouts, appObject)
  end
end

config()
local appWatcher = hs.application.watcher.new(applicationWatcher)
appWatcher:start()
hs.loadSpoon('ControlEscape'):start() -- Load Hammerspoon bits from https://github.com/jasonrudolph/ControlEscape.spoon
