" activate apple cmd buttom
set macmeta

" proper cmd-w behavior
" https://github.com/nathanaelkane/vim-command-w
macmenu &File.Close key=<nop>
nmap <D-w> :CommandW<CR>
imap <D-w> <Esc>:CommandW<CR>

colorscheme base16-solarized
